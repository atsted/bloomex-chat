import axios from 'axios'

const baseUrl = '/'

export default {
  send (input = {}, context = {}) {
    return axios.post(baseUrl, { input, context }).then(response => {
      return response.data
    })
  }
}
