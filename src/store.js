import Vue from 'vue'
import Vuex from 'vuex'
import api from '../api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    context: {},
    dialogue: []
  },
  getters: {
    dialogue (state) {
      return state.dialogue || []
    },
    context (state) {
      return state.context
    }
  },
  mutations: {
    UPDATE_DIALOGUE (state, { output, context }) {
      const id = state.dialogue.length
      const date = new Date()
      if (output.generic) {
        state.dialogue.push({ id, date, ...output })
      }
      context && (state.context = context)
    }
  },
  actions: {
    sendMessage ({ commit, getters }, message) {
      const input = { text: message }
      const context = getters.context
      if (message) {
        commit('UPDATE_DIALOGUE', {
          output: {
            name: 'Tester',
            generic: [{
              text: message,
              response_type: 'text'
            }]
          }
        })
      }
      api.send(input, context).then(({ output, context }) => {
        output.name = 'John'
        output.operator = true
        commit('UPDATE_DIALOGUE', { output, context })
      })
    }
  }
})
